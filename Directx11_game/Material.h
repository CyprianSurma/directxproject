#pragma once

#include <string>
#include <DirectXMath.h>
class Material
{  
public:
	std::string name
				,ambientTextureMap
				,diffuseTextureMap
				,specularTextureMap;
	DirectX::XMFLOAT3 ambientColor
					 ,diffuseColor
					 ,specularColor;
	float transparency=0;    // if not changed then default value is 0
	float specularExponent;	
	Material()noexcept {};
	Material(const Material &obj)noexcept;            
	void setDefaultValues()noexcept;                 
};

