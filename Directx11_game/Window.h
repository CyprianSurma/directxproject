#pragma once
#include<Windows.h>
 namespace Window_class {
	class Window
	{
	public:
		HWND & GetHwnd();
		bool ProcesInput() noexcept;
		Window(HINSTANCE);
		~Window();

		static const unsigned int X_pos = 20;
		static const unsigned int y_pos = 20;
		static const unsigned int Width = 200;
		static const unsigned int Height = 200;
		static const bool windowed =true;


	private:
		//private functions 
		static LRESULT WINAPI MsgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);



		//private variables 


		HINSTANCE hInst = nullptr;
		HWND hWnd = nullptr;
		static constexpr wchar_t* ClassName = L"Directx Game";

	};

}