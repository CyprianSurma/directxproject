#pragma once
#include<vector>
#include <fstream>
#include <string>
#include "Material.h"
#include <Windows.h>
#include <DirectXMath.h>
class Object_3D
{
public:

	std::vector<uint32_t >  indices;   //
	std::vector<DirectX::XMFLOAT3>  vertices
								   ,vertexNormals
								   ,textureCords;;
	std::vector<Material> materials;
	bool readFromFile_wavefrontOBJ(char*);                       
	
	Object_3D();
	~Object_3D();
private :	
	bool readMaterials(const char*filename);

};

