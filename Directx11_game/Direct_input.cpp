#include "Direct_input.h"
#include <dinput.h>
#include <memory>
#include <iostream>
Direct_input::Direct_input()
{
	directInput = nullptr;
	keyboard = nullptr;
	keyboard = nullptr;
	IsLmbPressed = false;
	IsRmbPressed = false;
}

Direct_input::~Direct_input()
{
}
bool Direct_input::Init(HINSTANCE hinstance, HWND hwnd,  int  Width,  int  Height)
{;
	HRESULT hr;
     
	Screen_Width = Width;
	Screen_Height = Height;
	
	//
	hr = DirectInput8Create(hinstance, DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&directInput, NULL);                           /* HRESULT DirectInput8Create(
																																								HINSTANCE hinst,
																																								DWORD dwVersion,
																																								REFIID riidltf,
																																								LPVOID * ppvOut,
																																								LPUNKNOWN punkOuter)*/
	if (FAILED(hr))
	{
		return false;
	}
	
	// initialize keyboard
	
	
	hr = directInput->CreateDevice(GUID_SysKeyboard, &keyboard, NULL);
	if (FAILED(hr))
	{
		return false;
	}
    hr = keyboard->SetDataFormat(&c_dfDIKeyboard);
	if (FAILED(hr))
	{
		return false;
	}
	hr = keyboard->SetCooperativeLevel(hwnd, DISCL_FOREGROUND | DISCL_EXCLUSIVE);                                                  //without sharing keybord with other programs  , require to be on foreground
	if (FAILED(hr))
	{
		return false;
	}
	
	
	hr = keyboard->Acquire();
	if (FAILED(hr))
	{
		return false;
	}

//	initialize mouse
	hr = directInput->CreateDevice(GUID_SysMouse, &mouse, NULL);
	if (FAILED(hr))
	{
		return false;
	}

	hr = mouse->SetDataFormat(&c_dfDIMouse);
	if (FAILED(hr))
	{
		return false;
	}


	hr = mouse->SetCooperativeLevel(hwnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE);   //sharing with other programs
	if (FAILED(hr))
	{
		return false;
	}


	hr = mouse->Acquire();
	if (FAILED(hr))
	{
		return false;
	}

	return true;


}


void Direct_input::Release()
{
	if (keyboard)
	{
		keyboard->Unacquire();
		keyboard->Release();
		keyboard = nullptr;
	}
	if (mouse)
	{
		mouse->Unacquire();
		mouse->Release();
		mouse = nullptr;
	}

	if (directInput)
	{
		directInput->Release();
		directInput = nullptr;
	}
	
}


bool Direct_input::CheckMouseState()
{
	HRESULT hr;
	hr=mouse->GetDeviceState(sizeof(DIMOUSESTATE), (void*)&mouse_state);
    
	if (FAILED(hr))
	             {
					 if ((hr == DIERR_NOTACQUIRED) || (hr == DIERR_INPUTLOST))
					 {
						 mouse->Acquire();
					 }
					 else
					 {
						 return false;
					 }
	             }
	return true;
}
bool Direct_input::CheckKeyboardState()
{
	HRESULT hr;
	hr = keyboard->GetDeviceState(sizeof(keyboard_state), (void*)&keyboard_state);

	if (FAILED(hr))
	{
		if ((hr == DIERR_NOTACQUIRED) || (hr == DIERR_INPUTLOST))
		{
			keyboard->Acquire();
		}
		else
		{
			return false;
		}
	}
	
	return true;
}

bool Direct_input::UpdateMouseQ()
{
	if (!CheckMouseState())
		return false;
	//Left mouse button
	if (mouse_state.rgbButtons[0] & 0x80) //if lmb is pressed
	{
		if (!IsLmbPressed)
		{              //If wasnt 
			MouseEventBuffer.push({ Mouse::LeftPress,mouse_state.lX,mouse_state.lY });      //push Event 
			IsLmbPressed = TRUE;
		}

	}   


	else                   //if lmb isnt pressed
	    {
		if (IsLmbPressed) 
		{
			MouseEventBuffer.push({ Mouse::LeftRelease,mouse_state.lX,mouse_state.lY });
			IsLmbPressed = false;
		}
		}
	
	



	//right mouse button


	if (mouse_state.rgbButtons[1] & 0x80) //if rmb is pressed
	{
		if (!IsRmbPressed)
		{              
			MouseEventBuffer.push({ Mouse::RightPress,mouse_state.lX,mouse_state.lY });     
			IsLmbPressed = true;
		}

	}


	else               
	{
		if (IsRmbPressed)
		{
			MouseEventBuffer.push({ Mouse::RightRelease,mouse_state.lX,mouse_state.lY });
			IsRmbPressed = false;
		}
	}
	//------------------------------------------------------------------------------
	//Mouse wheel
	
	if (mouse_state.lZ < 0)
	{
		MouseEventBuffer.push({ Mouse::WheelDown,mouse_state.lX,mouse_state.lY });
	}
	else if (mouse_state.lZ > 0)
	{
		MouseEventBuffer.push({ Mouse::WheelUp,mouse_state.lX,mouse_state.lY });
	}

	//-----------------------------------------------------------------------
	return true;
}

bool Direct_input::GetMouseEvent(Mouse& mouse)
{
	if (MouseEventBuffer.empty())
	{
		return false;
	}
	mouse = MouseEventBuffer.front();
	MouseEventBuffer.pop();
	return true;
}

bool Direct_input::UpdateKeyboardQ()
{
	if (!CheckKeyboardState())
		return false;
	
	for (auto i = DIK_ESCAPE; i != 256; ++i)
	{
		if (keyboard_state[i] & 0x80)//if key is pressed 
		{
			if (!KeyboardState[i]) //and it wasnt  
			{
				KeyboardBuffer.push({ Keyboard::KeyPress,(unsigned char)i });  //put keypress msg in queue
				KeyboardState[i] = true;
				
			}
		}
		else//if key isnt pressed
		{
			if(KeyboardState[i])//and it was
			KeyboardBuffer.push({Keyboard::KeyRelease, (unsigned char)i});  //put keyrelease msg in queue
			KeyboardState[i] = false;

		}
	

	}
	return true;
  

}
bool Direct_input::GetKeyboardEvent(Keyboard& Event)
{
	if (KeyboardBuffer.empty())
	{
		return false;
	}
	Event = KeyboardBuffer.front();
	KeyboardBuffer.pop();
	return true;
}