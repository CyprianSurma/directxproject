#include "Object_3D.h"
#include <DirectXMath.h>


Object_3D::Object_3D()
{
}


Object_3D::~Object_3D()
{
}

bool Object_3D::readFromFile_wavefrontOBJ(char* filename)
{
	unsigned char c;
	std::string mtlibPath
			  , currentMaterialName = ""
		      ,input;
	std::ifstream file;
	int smoothShading;
	
	
	file.open(filename, std::ios::in);
	if (!file.good())
	{
		return false;
	}
	
	
	
	
	while (!file.eof())
	{
		file >> input;
		if (input == "v")
		{
			DirectX::XMFLOAT3 v;
			file >> v.x;
			file >> v.y;
			file >> v.z;
			vertices.push_back(std::move(v));
		}
		if (input == "vt")
		{
			DirectX::XMFLOAT3 v;
			file >> v.x;
			file >> v.y;
			file >> v.z;
			textureCords.push_back(std::move(v));
		}
		if (input == "vn")
		{
			DirectX::XMFLOAT3 v;
			file >> v.x;
			file >> v.y;
			file >> v.z;
			vertexNormals.push_back(std::move(v));
		}
				
		
			if (input == "f")
		{
				for (int i = 0; i < 3; ++i)
				{
					uint32_t  v = 0
						, vt = 0
						, vn = 0;
					file >> v >> c;
					if (c == '/');
					{
						c = 'c';
						if (!(file >> vt))
						{
							file.clear();
							file >> c;
						}


						if (c == '/')
						{
							file >> vn;
						}
					}
					indices.push_back(std::move(v));
					indices.push_back(std::move(vt));
					indices.push_back(std::move(vn));
				}
			}

		
			if(input=="mtllib")
							{
									file >> mtlibPath;
									readMaterials(mtlibPath.c_str());
							}					
						
									
						
	}
    
	file.close();
	return true;

}
bool Object_3D::readMaterials(const char*filename)
{
	bool firstMaterial = true;
	std::string input;
    std::ifstream file;
	Material mat;
	char garbage;

	file.open(filename, std::ios::in);
	if (!file.good())
	{
		MessageBox(0, L"Unable to load material form file", 0, 0);
		return false;
	}

	while (file>>input)
	{	
		if (input == "#")
		{
			while ((garbage = file.get()) != '\n');
		}
		else if (input == "newmtl")
		{
			if (!firstMaterial)
			{
				materials.push_back(mat);
			}
			firstMaterial = false;
			mat.setDefaultValues();
			file >> mat.name;

		}
		else if (input[0] == 'K')
		{
			switch (input[1])
			{
			case 'a':
			{
				file >> mat.ambientColor.x;
				file >> mat.ambientColor.y;
				file >> mat.ambientColor.z;
				break;
			}
			case 'd':
			{
				file >> mat.diffuseColor.x;
				file >> mat.diffuseColor.y;
				file >> mat.diffuseColor.z;
				break;
			}
			case 's':
			{
				file >> mat.specularColor.x;
				file >> mat.specularColor.y;
				file >> mat.specularColor.z;
				break;
			}
			default:
				break;
			}
		}
		else if (input == "d")
		{
			file >> mat.transparency;
			mat.transparency = 1 - mat.transparency;
		}
		else if (input == "Tr")
		{
			file >> mat.transparency;			
		}
		else if (input == "Ns")
		{
			file >> mat.specularExponent;
		}	
		else if (input == "map_Ka")
		{
			file >> mat.ambientTextureMap;
		}
		else if (input == "map_Kd")
		{
			file >> mat.diffuseTextureMap;
		}
		else if (input == "map_Ks")
		{
			file >> mat.specularTextureMap;
		}

	}
	materials.push_back(mat);



	

	
}