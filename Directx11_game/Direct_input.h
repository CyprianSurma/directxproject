#pragma once
#include <dinput.h>
#include <queue>
#include <bitset>
#pragma comment(lib, "dinput8.lib")
#pragma comment(lib, "dxguid.lib")
class Direct_input
{
	public:
		
			class Mouse
			{
			public:
				
				enum Type
				{
					LeftPress,
					RightPress,
					LeftRelease,
					RightRelease,
					WheelUp,
					WheelDown,					
					Other
				};
				
				Type type;
				Mouse() :type(Other), x_position(0), y_position(0)				{};
				Mouse(Type type, const int & x_pos, const int& y_pos) :type(type), x_position(x_pos), y_position(y_pos) {};
			private:
				
			    int x_position;
				int y_position;
			};



			class Keyboard
			{
			public:
				
				
				enum Type
				{
					KeyPress,
					KeyRelease,
					Other
				};
				Keyboard() :type(Other), Key_Code(NULL) {};
				Keyboard(Type type,unsigned char  key_code) :type(type), Key_Code(key_code) {};
				unsigned char Key_Code;
				Type type;
			};
			
			
	    
	Direct_input();
	Direct_input(const Direct_input &) = delete;
	Direct_input& operator = (const Direct_input &) = delete;
	~Direct_input();
	bool Init(HINSTANCE, HWND,    int,   int);
	void Release();
	bool UpdateMouseQ();//update mouse buffer
	bool UpdateKeyboardQ();
	bool GetMouseEvent(Mouse &);
	bool GetKeyboardEvent(Keyboard&);
private: 
	//directinput interfaces
	IDirectInput8* directInput;
	IDirectInputDevice8* mouse;
	IDirectInputDevice8* keyboard;

	unsigned int Screen_Width;
	unsigned int Screen_Height;

	DIMOUSESTATE mouse_state;
	unsigned char keyboard_state[256];	//valuses received from direct device
	

	bool IsLmbPressed;
	bool IsRmbPressed;
	
	bool CheckMouseState();
	bool CheckKeyboardState();
 
	std::queue<Mouse> MouseEventBuffer;
	std::queue<Keyboard> KeyboardBuffer;
    std::bitset<256>  KeyboardState;    //states of keys before       constructor is setting all values as false
	};

