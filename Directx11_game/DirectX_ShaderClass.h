#pragma once

#include <directxmath.h>
#include <d3d11.h>
class DirectX_ShaderClass
{
public:
	
	//bool Initialize(ID3D11Device*, HWND);
//	bool Render(ID3D11DeviceContext*, int, DirectX::XMMATRIX, DirectX::XMMATRIX, DirectX::XMMATRIX);
private:
	ID3D11VertexShader* pvertexShader;
	ID3D11PixelShader* ppixelShader;
	ID3D11InputLayout* playout;
	ID3D11Buffer* pmatrixBuffer;

	DirectX_ShaderClass();
	~DirectX_ShaderClass();
};

