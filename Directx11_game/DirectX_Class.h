#pragma once
#include <d3d11.h>
#include<DirectXMath.h>
#include "Object_3D.h"
#include <d3d11.h>
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "d3dcompiler.lib")
class DirectX_Class
{
public:
	DirectX_Class();
	~DirectX_Class();
	DirectX_Class(const DirectX_Class &) = delete;                         
	DirectX_Class& operator = (const DirectX_Class &) = delete;
	bool Init(HWND);
	void Release();
	D3D11_INPUT_ELEMENT_DESC a;

private:

	IDXGISwapChain* pSwapChain;
	ID3D11Device* pD3DDevice;
	ID3D11DeviceContext* pD3DImmediateContext;
	ID3D11RenderTargetView * pRenderTargetView;
	ID3D11Texture2D* pDepthStencilBuffer;
    ID3D11DepthStencilView* pDepthStencilView;
			
};

