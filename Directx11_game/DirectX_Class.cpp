#include "DirectX_Class.h"
#include "Window.h"
#include <d3d11.h>
#include <d3dcompiler.h>

DirectX_Class::DirectX_Class()
{	
}


DirectX_Class::~DirectX_Class()
{ 
}

bool DirectX_Class::Init(HWND hwnd)
{   
	DXGI_SAMPLE_DESC MultisamplingSettings;
	D3D_FEATURE_LEVEL featureLevel= D3D_FEATURE_LEVEL_11_0;	
	D3D11_TEXTURE2D_DESC depthStencilDesc;	
	DXGI_SWAP_CHAIN_DESC sd;
	D3D11_VIEWPORT vp;
	//without multisampling 
	MultisamplingSettings.Count = 1;
	MultisamplingSettings.Quality = 0;
	//describe swap chain
	sd.BufferDesc.Width = Window_class::Window::Width;
	sd.BufferDesc.Height = Window_class::Window::Height;
	sd.BufferDesc.RefreshRate.Numerator = 60;          //refresh rate of back buffer
	sd.BufferDesc.RefreshRate.Denominator = 1;
	sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	sd.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	sd.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	
	sd.SampleDesc = MultisamplingSettings;
	
	sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;  //back buffer as render target
	sd.BufferCount = 1;
	sd.OutputWindow = hwnd;
	sd.Windowed = Window_class::Window::windowed;
	sd.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	sd.Flags = 0;
	
	//describe depth stencil
	
	depthStencilDesc.Width = Window_class::Window::Width;
	depthStencilDesc.Height = Window_class::Window::Height;
	depthStencilDesc.ArraySize = 1;
	depthStencilDesc.MipLevels = 1;
	depthStencilDesc.SampleDesc = MultisamplingSettings;
	depthStencilDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilDesc.Usage = D3D11_USAGE_DEFAULT;
	depthStencilDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthStencilDesc.CPUAccessFlags = 0;
	depthStencilDesc.MiscFlags = 0;

	
	//describe viewport
	vp.Height= static_cast<float>(Window_class::Window::Height);
	vp.Width= static_cast<float>(Window_class::Window::Width);
	vp.TopLeftX = static_cast<float>(Window_class::Window::X_pos);
	vp.TopLeftY = static_cast<float>(Window_class::Window::y_pos);
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;
	
	
	UINT Flags = 0;
#if defined(DEBUG)||defined(_DEBUG)   
	//Flags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	HRESULT hr;
	//D3D Device//
	hr = D3D11CreateDevice(0, D3D_DRIVER_TYPE_HARDWARE, 0, Flags, 0, 0, D3D11_SDK_VERSION, &pD3DDevice, 0, &pD3DImmediateContext);
		
		if (FAILED(hr))
		{
			MessageBox(0, L"Failed to create d3d device", 0, 0);
				return false;
		}
		//-------------------------------------------------------------------------------------------------------------------------------//
		//Swap Chain//

		//create IDXGIFactory needed to create Swap Chain
		IDXGIDevice * pIDXGIDevice = nullptr;
		hr = pD3DDevice->QueryInterface(__uuidof(IDXGIDevice), (void **)& pIDXGIDevice);
		if (FAILED(hr))
		{
			{
				MessageBox(0, L"Failed to create IDXGIFactory", 0, 0);
				return false;
			}
		}

		IDXGIAdapter * pIDXGIAdapter = nullptr;
		hr = pIDXGIDevice->GetParent(__uuidof(IDXGIAdapter), (void **)& pIDXGIAdapter);
		if (FAILED(hr))
		{
				{
					MessageBox(0, L"Failed to create IDXGIFactory", 0, 0);
					return false;
				}
		}

		IDXGIFactory * pIDXGIFactory = nullptr;
		hr = pIDXGIAdapter->GetParent(__uuidof(IDXGIFactory), (void **)& pIDXGIFactory);
		if (FAILED(hr))
		{
			{
				MessageBox(0, L"Failed to create IDXGIFactory", 0, 0);
				return false;
			}
		}
		
		//create Swap Chain
		hr = pIDXGIFactory->CreateSwapChain(pD3DDevice, &sd, &pSwapChain);
		if (FAILED(hr))
		{
			{
				MessageBox(0, L"Failed to create Swap Chain", 0, 0);
				return false;
			}
		}

		//Realease com interfaces
		pIDXGIAdapter->Release();
		pIDXGIDevice->Release();
		pIDXGIFactory->Release();
		pIDXGIAdapter = nullptr;
		pIDXGIDevice = nullptr;
		pIDXGIFactory = nullptr;

		//------------------------------------------------------------------------------------//

		//Render Target View//
		// Get the pointer to the back buffer.
		ID3D11Texture2D * pBackBuffer;
		hr = pSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&pBackBuffer);
		if (FAILED(hr))
		{
			MessageBox(0, L"Failed to create Render Target View", 0, 0);
			return false;
		}
		//create render target view
		hr=pD3DDevice->CreateRenderTargetView(pBackBuffer, 0, &pRenderTargetView);
		if (FAILED(hr))
		{
			MessageBox(0, L"Failed to create Render Target View", 0, 0);
			return false;
		}

		pBackBuffer->Release();
		pBackBuffer = nullptr;
		//--------------------------------------------------------------------------------------------//

		//create depth/stencil buffer //

		hr = pD3DDevice->CreateTexture2D(&depthStencilDesc, 0, &pDepthStencilBuffer);
		if (FAILED(hr))
		{
			MessageBox(0, L"Failed to create Depth Stencil Buffer", 0, 0);
			return false;
		}

		hr = pD3DDevice->CreateDepthStencilView(pDepthStencilBuffer, 0, &pDepthStencilView);
		if (FAILED(hr))
		{
			MessageBox(0, L"Failed to create Depth Stencil View", 0, 0);
			return false;
		}

		//bind depth stencil view and render target
		pD3DImmediateContext->OMSetRenderTargets(1, &pRenderTargetView, pDepthStencilView);

		//create viewport
		pD3DImmediateContext->RSSetViewports(1, &vp);

		return true;
		

}


void DirectX_Class::Release()
{  
	
	if(pSwapChain)
	pSwapChain->Release();
	if (pD3DDevice)
	pD3DDevice->Release();
	if (pD3DImmediateContext)
	pD3DImmediateContext->Release();
	if (pRenderTargetView)
	pRenderTargetView->Release();
	if (pDepthStencilBuffer)
	pDepthStencilBuffer->Release();
	if (pDepthStencilView)
	pDepthStencilView->Release();
	
	
}