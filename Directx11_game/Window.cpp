#include "Window.h"
#include <Windows.h>
#include<string>
#include <fstream>
namespace Window_class {
	Window::Window(HINSTANCE hInst) :hInst(hInst)
	{
		RECT wr = { X_pos,y_pos,Width,Height };                     //Initializes a new instance of the Rect structure that has the specified x-coordinate, y-coordinate, width, and height.


		WNDCLASSEX wc = { sizeof(WNDCLASSEX),CS_CLASSDC,MsgProc,0,0,
		hInst,NULL,NULL,NULL,NULL,
			ClassName,NULL };
		RegisterClassEx(&wc);


		AdjustWindowRect(&wr, WS_OVERLAPPEDWINDOW, FALSE);
		hWnd = CreateWindowW(ClassName, L"Directx11 Game",
			WS_OVERLAPPEDWINDOW, wr.left, wr.top, wr.right - wr.left, wr.bottom - wr.top,
			NULL, NULL, hInst, NULL);

		ShowWindow(hWnd, SW_SHOWDEFAULT);
		UpdateWindow(hWnd);


	}

	
	Window::~Window()
	{
		UnregisterClass(ClassName, hInst);
	}
	LRESULT WINAPI Window::MsgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
	{
		switch (msg)
		{
	
		case WM_SIZING:
		{
		}
		break;

		}
		return DefWindowProc(hWnd, msg, wParam, lParam);
	}
	bool Window::ProcesInput() noexcept
	{
		MSG msg;
		while (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
			if (msg.message == WM_QUIT)
			{
				return false;
			}
		}

		return true;

	}
	HWND &Window::GetHwnd()
	{
		return hWnd;
	}
}