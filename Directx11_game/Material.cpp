#include "Material.h"
Material::Material(const Material &obj)noexcept:
	name(obj.name),
	ambientTextureMap(obj.ambientTextureMap),
	diffuseTextureMap(obj.diffuseTextureMap),
	specularTextureMap(obj.specularTextureMap),
	ambientColor(obj.ambientColor),
	diffuseColor(obj.diffuseColor),
	specularColor(obj.specularColor),
	transparency(obj.transparency),
	specularExponent(obj.specularExponent)
{};
	void Material::setDefaultValues()noexcept
	{
		name.clear();
		ambientTextureMap.clear();
		diffuseTextureMap.clear();
		specularTextureMap.clear();
		memset(&ambientColor, 0, sizeof(DirectX::XMFLOAT3));
		memset(&diffuseColor, 0, sizeof(DirectX::XMFLOAT3));
		memset(&specularColor, 0, sizeof(DirectX::XMFLOAT3));
		transparency = 0.0f;
		specularExponent = 0.0f;
	}